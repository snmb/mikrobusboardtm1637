/*********************************************************
 TM1637 Driver for 4 digit big led display
  author Tumanov Stanislav Aleksandrovich,
 Russia, Kirov city, maintumanov@mail.ru
     20.10.2021      	Основано на оригинальной библиотеке "Grove_4Digital_Display" от Frankie.Chu
    https://github.com/Seeed-Studio/Grove_4Digital_Display
 *********************************************************/

#ifndef TM1637_DIO_PIN
#define   TM1637_DIO_PIN         PIN_C4
#endif

#ifndef TM1637_CLK_PIN
#define   TM1637_CLK_PIN         PIN_C3
#endif

#define TM1637_ADDR_AUTO  0x40
#define TM1637_ADDR_FIXED 0x44
#define TM1637_DELAY_US   (10) 
#define TM1637_DEFAULT_BRIGHTNESS (2)
#define TM1637_SIZE56CLOCK  0x00
#define TM1637_SIZE36  0x01


#define TM1637_SPACE 10
#define TM1637_DASH 11
#define TM1637_A 12
#define TM1637_b 13
#define TM1637_C 14
#define TM1637_d 15
#define TM1637_E 16
#define TM1637_F 17
#define TM1637_G 18
#define TM1637_H 19
#define TM1637_I 20
#define TM1637_J 21
#define TM1637_L 22
#define TM1637_n 23
#define TM1637_o 24
#define TM1637_q 25
#define TM1637_r 26
#define TM1637_t 27
#define TM1637_P 28
#define TM1637_S 29
#define TM1637_U 30
#define TM1637_Y 31
#define TM1637_w 32
#define TM1637_CH 33 //русская буква Ч
#define TM1637_RG 34 //русская буква Г
#define TM1637_RE 35 //русская буква Э
#define TM1637_Rd 36 //русская буква д
#define TM1637_RB 37 //русская буква Б
#define TM1637_RZ 38 //русская буква З
#define TM1637_RP 39 //русская буква П
#define TM1637_DEGREE 40
#define TM1637_UNDER 41
#define TM1637_PERCENT 42

static unsigned int8 TM1637_TubeTab[] = {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,
                                         0x7f,0x6f,0x00,0x40,0x77,0x7c,0x39,0x5e,
                                         0x79,0x71,0x3d,0x76,0x06,0x1e,0x38,0x54,
                                         0x5c,0x67,0x50,0x78,0x73,0x6d,0x3e,0x6e,
                                         0x1D,0x66,0x31,0x4f,0x67,0x7d,0x4f,0x37,
                                         0x63,0x08, 0x49};

unsigned int8 TM1637_private_dispCtrl = 0x88 + TM1637_DEFAULT_BRIGHTNESS; //7 brightness
unsigned int8 TM1637_private_segment[4];
unsigned int8 TM1637_private_point = 0;
unsigned int8 TM1637_private_dispType = 0;
int1 TM1637_private_clockpoint = 0;

//====================== public =================
void TM1637_displayType(unsigned int8 type);
void TM1637_displayClock(unsigned int8 hrs, unsigned int8 mins);				// выводит часы и минуты
void TM1637_displayInt(signed int16 value, unsigned int8 simbol = 0); 				// выводит число от -999 до 9999 (да, со знаком минус)
void TM1637_displayHumidity(unsigned int8 value);
void TM1637_displayTemperature(float value); 
void TM1637_displayText(int8 simb0, int8 simb1, int8 simb2, int8 simb3);
void TM1637_displayBright(unsigned int8 brightness, int1 update);
void TM1637_displayClear();

//====================== private ================
void TM1637_start();
void TM1637_stop();
void TM1637_writeByte(unsigned int8 data); 
void TM1637_displayUpdate(); 
void TM1637_coding(unsigned int8 *dispData, unsigned int8 point);

void TM1637_start() {
    output_drive(TM1637_CLK_PIN);
    output_drive(TM1637_DIO_PIN);
    output_high(TM1637_CLK_PIN); //send start signal to TM1637
    delay_us(1);
    output_high(TM1637_DIO_PIN);
    delay_us(1);
    output_low(TM1637_DIO_PIN);
    delay_us(1);
    output_low(TM1637_CLK_PIN);
    delay_us(1);
}

void TM1637_stop() {
    output_low(TM1637_CLK_PIN);
    delay_us(1);
    output_low(TM1637_DIO_PIN);
    delay_us(1);
    output_high(TM1637_CLK_PIN);
    delay_us(1);
    output_high(TM1637_DIO_PIN);
}

void TM1637_writeByte(unsigned int8 data) {
    unsigned int8 i, count1;
    for (i = 0; i < 8; i ++) //sent 8bit data
    {
        output_low(TM1637_CLK_PIN);
        if (data & 0x01) output_high(TM1637_DIO_PIN); //LSB first
        else output_low(TM1637_DIO_PIN);
        data >>= 1;
        output_high(TM1637_CLK_PIN);
        delay_us(1);
    }
    output_low(TM1637_CLK_PIN);  //wait for the ACK
    delay_us(1);
    output_high(TM1637_DIO_PIN);
    delay_us(1);
    output_high(TM1637_CLK_PIN);
    delay_us(1);
    output_float(TM1637_DIO_PIN);
    delay_us(TM1637_DELAY_US);
    while(INPUT(TM1637_DIO_PIN))
    {
        count1 += 1;
        if(count1 == 200)//
        {
            output_drive(TM1637_DIO_PIN);
            output_low(TM1637_DIO_PIN);
            count1 = 0;
        }
        output_float(TM1637_DIO_PIN);
    }
    output_drive(TM1637_DIO_PIN);
}

void TM1637_displayUpdate() {
    unsigned int8 SegData[4];
    unsigned int8 i;
    for(i = 0; i < 4; i ++) SegData[i] = TM1637_private_segment[i];
    TM1637_coding(SegData, TM1637_private_point);
    TM1637_start();          //start signal sent to TM1637 from MCU
    TM1637_writeByte(TM1637_ADDR_AUTO);
    TM1637_stop();           
    TM1637_start();          
    TM1637_writeByte(0xc0);
    for(i=0; i < 4; i ++) TM1637_writeByte(SegData[i]);        
    TM1637_stop();           
    TM1637_start();          
    TM1637_writeByte(TM1637_private_dispCtrl);
    TM1637_stop();           
}

void TM1637_coding(unsigned int8 *dispData, unsigned int8 point) {
    unsigned int8 PointData;
    PointData = 0x00;
    for(unsigned int8 i = 0; i < 4; i ++) {
        if (point - 1 == i) PointData = 0x80;
        else PointData = 0x00;
        if(dispData[i] == 0x7f) dispData[i] = 0x00;
        else dispData[i] = TM1637_TubeTab[dispData[i]] + PointData;
    }
}

//=======================================================================
void TM1637_displayType(unsigned int8 type) {
    TM1637_private_dispType = type;
    if (TM1637_private_dispType > 1) TM1637_private_dispType = 1;
}

void TM1637_displayClock(unsigned int8 hrs, unsigned int8 mins) {
    if (hrs > 99 || mins > 99) return;
    if ((hrs / 10) == 0) TM1637_private_segment[0] = 10;
    else TM1637_private_segment[0] = (hrs / 10);
    TM1637_private_segment[1] = hrs % 10;
    TM1637_private_segment[2] = mins / 10;
    TM1637_private_segment[3] = mins % 10;
    TM1637_private_clockpoint = !TM1637_private_clockpoint;
    TM1637_private_point = TM1637_private_clockpoint?2:0;
    TM1637_displayUpdate();
}

void TM1637_displayInt(signed int16 value, unsigned int8 simbol) {
    int1 negative = 0;
    if (value < 0) negative = 1;
    if (value > 9999 || value < -999)  {
      TM1637_displayText(TM1637_DASH, TM1637_DASH, TM1637_DASH, TM1637_DASH);
      return;
    }
    unsigned int16 vol = abs(value);
    TM1637_private_segment[0] = vol / 1000;      	// количесто тысяч в числе
    unsigned int16 b = (unsigned int8)TM1637_private_segment[0] * 1000; 	// вспомогательная переменная
    TM1637_private_segment[1] = (vol - b) / 100; 	// получем количество сотен
    b += TM1637_private_segment[1] * 100;               	// суммируем сотни и тысячи
    TM1637_private_segment[2] = (unsigned int8)(vol - b) / 10;  	// получем десятки
    b += TM1637_private_segment[2] * 10;                	// сумма тысяч, сотен и десятков
    TM1637_private_segment[3] = (unsigned int8)vol - b;              	// получаем количество единиц

    if (vol < 10) {TM1637_private_segment[2] = negative?TM1637_DASH:TM1637_SPACE; negative = 0; }
    if (vol < 100) {TM1637_private_segment[1] = negative?TM1637_DASH:TM1637_SPACE; negative = 0;}
    if (vol < 1000) TM1637_private_segment[0] = negative?TM1637_DASH:(simbol?simbol:TM1637_SPACE);

    TM1637_private_point = 0;
    TM1637_displayUpdate();
}

void TM1637_displayHumidity(unsigned int8 value) {

    if (value > 100) {
      TM1637_displayText(TM1637_DASH, TM1637_DASH, TM1637_DASH, TM1637_PERCENT);
      return;
    }
    TM1637_private_segment[0] = value / 100;      	
    unsigned int8 b = (unsigned int8)TM1637_private_segment[0] * 100; 	
    TM1637_private_segment[1] = (value - b) / 10; 
    b += TM1637_private_segment[1] * 10;               
    TM1637_private_segment[2] = (unsigned int8)(value - b);  	
    if (value < 10) TM1637_private_segment[1] = TM1637_SPACE;
    if (value < 100) TM1637_private_segment[0] = TM1637_SPACE;
    TM1637_private_segment[3] = TM1637_PERCENT;
    TM1637_private_point = 0;
    TM1637_displayUpdate();
}

void TM1637_displayTemperature(float value) {
    unsigned int8 neg_seg = 0;
    signed int16 vol;
    int1 isFractional = true;
    if (TM1637_private_dispType == TM1637_SIZE56CLOCK) isFractional = false;
    TM1637_private_point = 0;
    if (value < -99 || value > 999) {
      TM1637_displayText(TM1637_DASH, TM1637_DASH, TM1637_DASH, TM1637_DEGREE);
      return;
    }
    // Выделение десятичных
      if ((value > -10) && (value < 100) && isFractional) {
          vol = (value * 10);
          TM1637_private_point = 2;
      } else vol = value;

    if (value < 0) neg_seg = TM1637_DASH; else neg_seg = 0;
    vol = abs(vol);
    TM1637_private_segment[0] = vol / 100; 	// получем количество сотен
    vol = vol - (signed int16)TM1637_private_segment[0] * 100;
    TM1637_private_segment[1] = vol / 10;
    vol = vol - (signed int16)TM1637_private_segment[1] * 10;
    TM1637_private_segment[2] = vol;

    DUISendUInt8(10, TM1637_private_segment[0]);

    if (!TM1637_private_segment[1] && !TM1637_private_segment[0] && !isFractional) {
      TM1637_private_segment[1] = neg_seg?neg_seg:TM1637_SPACE;
      TM1637_private_segment[0] = TM1637_SPACE;
    } else if (TM1637_private_segment[0] == 0) TM1637_private_segment[0] = neg_seg?neg_seg:TM1637_SPACE;

    TM1637_private_segment[3] = TM1637_DEGREE;
    TM1637_displayUpdate();
}

void TM1637_displayBright(unsigned int8 brightness, int1 update) {
    TM1637_private_dispCtrl = 0x88 + (brightness & 0x07);
    if (update) TM1637_displayUpdate();
}

void TM1637_displayText(int8 simb0, int8 simb1, int8 simb2, int8 simb3) {
  TM1637_private_segment[0] = simb0;
  TM1637_private_segment[1] = simb1;
  TM1637_private_segment[2] = simb2;
  TM1637_private_segment[3] = simb3;
  TM1637_private_point = 0;
  TM1637_displayUpdate();
}

void TM1637_displayClear() {
  for(unsigned int8 i = 0; i < 4; i ++) TM1637_private_segment[i] = TM1637_SPACE;
  TM1637_private_point = 0;
  TM1637_displayUpdate();
}
