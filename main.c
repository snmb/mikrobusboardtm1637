#include "main.h"
#include "dui.c"
#include "tm1637digit4.c"

 unsigned int8 hour = 0;
 unsigned int8 min = 0;
 unsigned int8 simb = 10; 

//================================================================
//TASK MESSAGE CHECK

#task(rate = 10 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//============================================================

void task_DUIcheck() {
   DUICheck();
}
void formInit(void) {
   DUIAddEditUint8(0, 5, 0, 7, (char*)"Bright");
   DUIAddEditUint8(5, 5, 10, 39, (char*)"Simb");
   DUIAddSliderInt16(1, 0, -999, 9999, (char*)"Number");
   DUIAddEditInt16(2, 0, -1000, 10000, (char*)"Temp");
   DUIAddEditUint8(6, 45, 0, 101, (char*)"влажность");
   DUIAddSliderUint8(3, 5, 0, 23, (char*)"Hour");
   DUIAddSliderUint8(4, 5, 0, 59, (char*)"Min");
   DUIAddLabel(10, (char*)"DEBUG");
   DUIAddButton(11, (char*)"clear");
   DUIAddButton(12, (char*)"wifi");
   DUIAddButton(13, (char*)"init");
}

void formRead(unsigned int8 channel, unsigned int8* data) {
   switch (channel) {
      case 0: TM1637_displayBright(DUIGetInt8(), true); break;
      case 1: TM1637_displayInt(DUIGetInt16()); break;
      case 2: 
         float t = DUIGetInt16();
         TM1637_displayTemperature(t / 10); break;
      case 3:
         hour = DUIGetUInt8();
         TM1637_displayClock(hour, min); 
         break;
      case 4:
         min = DUIGetUInt8();
         TM1637_displayClock(hour, min); 
         break;      
      case 5:
         simb = DUIGetUInt8();
         TM1637_displayInt(simb, simb);
         break;  
      case 6: TM1637_displayHumidity(DUIGetUInt8()); break;   
      case 11: TM1637_displayClear(); break;
      case 12: TM1637_displayText(TM1637_w, TM1637_I, TM1637_F, TM1637_I); break;
      case 13: TM1637_displayText(TM1637_I, TM1637_n, TM1637_I, TM1637_t); break;
   }
}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x80 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);
    TM1637_displayType(TM1637_SIZE36);
}

void main() {
   initialization();
   rtos_run();
}
